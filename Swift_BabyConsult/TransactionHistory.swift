//
//  TransactionHistory.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 16/12/22.
//

import UIKit
import Alamofire

class TransactionHistory: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var historyBackButton: UIButton!
    @IBOutlet weak var pageNameLabel: UILabel!
    @IBOutlet weak var tableViewRiwayat: UITableView!
    
    var arrayItem: [[String : Any]] = [[:]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        tableViewRiwayat.delegate = self
        tableViewRiwayat.dataSource = self
        pageNameLabel.text = "Riwayat Transaksi"
        historyBackButton.addTarget(self, action: #selector(buttonBackHistory), for: .touchUpInside)
        getRequestHistoryTransaction()
        
    }
    
    @objc func buttonBackHistory() {
        self.navigationController?.popViewController(animated: true)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayItem.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
        let labelTanggal = cell.viewWithTag(1) as! UILabel
        let labelNamaDokter = cell.viewWithTag(2) as! UILabel
        let labelTotalHarga = cell.viewWithTag(3) as! UILabel
        let labelJenisKelamin = cell.viewWithTag(4) as! UILabel
        let labelAsalRumahSakit = cell.viewWithTag(5) as! UILabel
        let labelKategori = cell.viewWithTag(6) as! UILabel
        
        let namaDokterLabel = cell.viewWithTag(7) as! UILabel
        let jenisKelaminLabel = cell.viewWithTag(8) as! UILabel
        let asalRumahSakitLabel = cell.viewWithTag(9) as! UILabel
        let kategoriLabel = cell.viewWithTag(10) as! UILabel
        
        namaDokterLabel.text = "Nama Dokter    : "
        jenisKelaminLabel.text = "Jenis Kelamin    : "
        asalRumahSakitLabel.text = "Rumah Sakit      : "
        kategoriLabel.text = "Kategori           : "
        
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.black.cgColor
        
        let itemLabel = arrayItem[indexPath.row]
        let tanggalTransaksi = itemLabel["tanggalTransaksi"] as? String
        let dokter = itemLabel["doctor"] as? [String : Any]
        let namaDokter = dokter?["namaDokter"] as? String
        let jenisKelamin = dokter?["jenisKelamin"] as? String
        let asalRumahSakit = dokter?["asalRumahSakit"] as? String
        
        let categoryDoctor  = dokter?["doctorCategory"] as? [String : Any]
        let kategori = categoryDoctor?["nama_kategori"] as? String
        let totalHarga = itemLabel["totalHarga"] as? Int ?? 0
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
                formatter.groupingSeparator = "."
                formatter.numberStyle = .decimal
        
        let formattedNominalTargetAmount = formatter.string(from: totalHarga as! NSNumber) ?? ""
        
        labelTanggal.text = tanggalTransaksi
        labelNamaDokter.text = namaDokter
        labelTotalHarga.text = "Rp \(formattedNominalTargetAmount)"
        labelJenisKelamin.text = jenisKelamin
        labelAsalRumahSakit.text = asalRumahSakit
        labelKategori.text = kategori
        
        return cell
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
        
    }
    
    func getRequestHistoryTransaction(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/baby-consult/transaction-history/current-user", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any]
                let code = itemObject?["status"] as? Int
                let data = itemObject?["data"] as? [[String : Any]]
                self.arrayItem = data!
                self.tableViewRiwayat.reloadData()
                
                if code ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                }
                
                print(itemObject!)
            
            case .failure(let error):
                print(error)
            }
        })

    }

}
