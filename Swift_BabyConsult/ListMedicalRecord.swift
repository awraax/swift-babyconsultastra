//
//  ListMedicalRecord.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 27/12/22.
//

import UIKit
import Alamofire

class ListMedicalRecord: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var buttonBackListMedical: UIButton!
    @IBOutlet weak var pageNameLabel: UILabel!
    @IBOutlet weak var listMedicalRecordTV: UITableView!
    
    var arrayItem: [[String : Any]] = [[:]]
//    var idRecord: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        listMedicalRecordTV.delegate = self
        listMedicalRecordTV.dataSource = self
        pageNameLabel.text = "Medical Record"
        buttonBackListMedical.addTarget(self, action: #selector(buttonBackListMedHis), for: .touchUpInside)
        getRequestMedicalHistory()

    }
    
    @objc func buttonBackListMedHis(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrayItem.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellhistory", for: indexPath)
        let tanggalPemeriksaanLabel = cell.viewWithTag(1) as! UILabel
        let keluhanPasienLabel = cell.viewWithTag(2) as! UILabel
        let diagnosisPasienLabel = cell.viewWithTag(3) as! UILabel
        let pengobatanPasienLabel = cell.viewWithTag(4) as! UILabel
        let keluhanLabel = cell.viewWithTag(5) as! UILabel
        let diagnosisLabel = cell.viewWithTag(6) as! UILabel
        let pengobatanLabel = cell.viewWithTag(7) as! UILabel
        let medicalView = cell.viewWithTag(8)!
        
        medicalView.layer.borderWidth = 0.5
        medicalView.layer.borderColor = UIColor.black.cgColor
        medicalView.layer.cornerRadius = 5.0
        
        keluhanLabel.text = "Keluhan        : "
        diagnosisLabel.text = "Diagnosis     : "
        pengobatanLabel.text = "Pengobatan : "
        
        let itemLabel = arrayItem[indexPath.row]
        let keluhanPasein = itemLabel["keluhanPasien"] as? String
        let rekamMedisPasien = itemLabel["record"] as? [String : Any]
        let tanggalPemeriksaan = rekamMedisPasien?["tanggalPemeriksaan"] as? String
        let diagnosisPasien = rekamMedisPasien?["diagnosis"] as? String
        let pengobatanPasien = rekamMedisPasien?["pengobatan"] as? String

        keluhanPasienLabel.text = keluhanPasein
        tanggalPemeriksaanLabel.text = tanggalPemeriksaan
        diagnosisPasienLabel.text = diagnosisPasien
        pengobatanPasienLabel.text = pengobatanPasien
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    func getRequestMedicalHistory(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/baby-consult/medical-record/current-user", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: {response in switch response.result {
            case .success:
                let itemObject = response.value as? [String : Any] ?? [:]
                let code = itemObject["status"] as? Int
                let data = itemObject["data"] as? [[String : Any]]
                self.arrayItem = data!
                self.listMedicalRecordTV.reloadData()
            
                if code ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                }
                
                print(itemObject)
                
            case .failure(let error) :
                print(error)
            }
                
            })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let medicalRecord = self.storyboard?.instantiateViewController(withIdentifier: "MedicalRecordController") as! MedicalRecordController

        self.navigationController?.pushViewController(medicalRecord, animated: true)
    }
}
