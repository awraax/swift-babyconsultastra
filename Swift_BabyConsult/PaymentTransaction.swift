//
//  PaymentTransaction.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 16/12/22.
//

import UIKit
import Alamofire


class PaymentTransaction: UIViewController {

    @IBOutlet weak var usernameSuccess: UILabel!
    @IBOutlet weak var noTelpSuccess: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var data: [String: Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        nextButton.addTarget(self, action: #selector(buttonNextTapped), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(buttonBackTap), for: .touchUpInside)

        getRequestUserSuccess()
    }
    
    func getRequestUserSuccess(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/baby-consult/current_user", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any] ?? [:]
                let code = itemObject["status"] as? Int

                if code ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                } else {
                    let dataProfile = itemObject["data"] as? [String : Any]
                    let usernameData = dataProfile!["username"] as? String
                    let noTelpData = dataProfile!["noTelp"] as? String
                    
                    self.usernameSuccess.text = usernameData
                    self.noTelpSuccess.text = noTelpData
                    
                }
                
                print(itemObject)
            
            case .failure(let error):
                print(error)
            }
        })
    }
    
    @objc func buttonNextTapped(){
        let makeConsultationForum = self.storyboard?.instantiateViewController(withIdentifier: "ConsultationForum")

        self.navigationController?.pushViewController(makeConsultationForum!, animated: true)
    }
     
    @objc func buttonBackTap(){
        self.navigationController?.popViewController(animated: true)
    }
    

}
