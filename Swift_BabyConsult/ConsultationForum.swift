//
//  ConsultationForum.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 16/12/22.
//

import UIKit
import Alamofire

class ConsultationForum: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var consultationMessage: UITextView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var chatTableView: UITableView!
    
    var arrMessage: [[String: Any]] = []
    var messageItem = ""
    
    override func viewDidLoad()  {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        chatTableView.delegate = self
        chatTableView.dataSource = self
        
        consultationMessage.delegate = self
        consultationMessage.text = "Ketikkan Pesanmu Disini ..."
        consultationMessage.textColor = UIColor.lightGray
        consultationMessage.returnKeyType = .done
        
       
//        consultationMessage.isScrollEnabled = true
//        consultationMessage.text = "Ketik Pesanmu Di Sini"
//        consultationMessage.textColor = UIColor.lightGray
//        consultationMessage.sizeToFit()
//        consultationMessage.layer.borderWidth = 0.5
//        consultationMessage.layer.borderColor = UIColor.black.cgColor
//        consultationMessage.layer.cornerRadius = 10

//        constrainData.constant = self.consultationMessage.contentSize.height
        
        buttonBack.addTarget(self, action: #selector(buttonBackConsultation), for: .touchUpInside)
        sendButton.addTarget(self, action: #selector(buttonSendConsultation), for: .touchUpInside)
        
        getRequestMessage()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if consultationMessage.text == "Ketikkan Pesanmu Disini ..."{
            consultationMessage.text = ""
            consultationMessage.textColor = UIColor.black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            consultationMessage.resignFirstResponder()
        }
        return true
    }
    
    
//    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
//            self.view.endEditing(true)
//        }
//
//        func textViewDidBeginEditing(_ textView: UITextView) {
//            if consultationMessage.textColor == UIColor.lightGray {
//                consultationMessage.text = nil
//                consultationMessage.textColor = UIColor.black
//            }
//        }
//
//        func textViewDidEndEditing(_ textView: UITextView) {
//            if consultationMessage.text.isEmpty {
//                consultationMessage.text = "Ketik Pesanmu Di Sini"
//                consultationMessage.textColor = UIColor.lightGray
//            }
//        }

    
    @objc func buttonBackConsultation(){
        let backButton = self.storyboard?.instantiateViewController(withIdentifier: "HomepageController")
        self.navigationController?.pushViewController(backButton!, animated: true)
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var itemMessage = arrMessage[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! PatientProblemTableViewCell
        self.messageItem = itemMessage["keluhanPasien"] as! String
        cell.detailProblem.text = messageItem
        cell.patientLabel.text = "Parents"
            
        return cell
    }
    
    @objc func buttonSendConsultation(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["keluhanPasien": consultationMessage.text]
        
        AF.request("http://localhost:8080/baby-consult/patient-problem/current-user", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any]
                let code = itemObject?["status"] as? Int
                let data = itemObject?["data"] as? [[String : Any]]
                let message = itemObject?["keluhanPasien"] as? String

                if code ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                }
                self.chatTableView.reloadData()
                self.getRequestMessage()
                self.consultationMessage.text = ""
            
            case .failure(let error):
                print(error)
            }
        })
        
    }
    
    func getRequestMessage(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]

        AF.request("http://localhost:8080/baby-consult/patient-problem/doctor/current-user", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any]
                let code = itemObject?["status"] as? Int
                let data = itemObject?["data"] as? [[String : Any]]
                let message = itemObject?["keluhanPasien"] as? String

                self.arrMessage = data ?? []
                print(self.arrMessage)

                if code ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                }

                self.chatTableView.reloadData()

            case .failure(let error):
                print(error)
            }
        })
    }
//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessage.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
//        return self.messageItem.height(withConstrainedWidth: UIScreen.main.bounds.size.width - 80, font: UIFont.systemFont(ofSize: 15)) + 97
    }

    
}
