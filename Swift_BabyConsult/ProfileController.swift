//
//  ProfileController.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 21/12/22.
//

import UIKit
import Alamofire

class ProfileController: UIViewController {
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var saldoView: UIView!
    
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var pageNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var noTelp: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var saldo: UILabel!
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var noTelpLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var saldoLabel: UILabel!
    
    var data: [String: Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        pageNameLabel.text = "Profile"
        logOutButton.addTarget(self, action: #selector(buttonLogOutTapped), for: .touchUpInside)
        buttonBack.addTarget(self, action: #selector(buttonBackProfile), for: .touchUpInside)
        
        profileView.layer.borderWidth = 0.5
        profileView.layer.borderColor = UIColor.black.cgColor
        profileView.layer.cornerRadius = 5.0
        
        saldoView.layer.borderWidth = 0.5
        saldoView.layer.borderColor = UIColor.black.cgColor
        saldoView.layer.cornerRadius = 5.0
        
        buttonBack.layer.cornerRadius = 5.0
        
        usernameLabel.text = "Username  :"
        roleLabel.text = "Role          :"
        emailLabel.text = "Email         :"
        noTelpLabel.text = "No Telp     :"
        saldoLabel.text = "Saldo"
        
        getRequestUser()
    }
    
    
    @objc func buttonBackProfile(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getRequestUser(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/baby-consult/current_user", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any] ?? [:]
                let code = itemObject["status"] as? Int

                
                if code ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                } else {
                    let dataProfile = itemObject["data"] as? [String : Any]
                    let usernameData = dataProfile!["username"] as? String
                    let emailData = dataProfile!["email"] as? String
                    let noTelpData = dataProfile!["noTelp"] as? String
                    let saldoData = dataProfile?["wallet"] as? [String : Any]
                    let wallet = saldoData?["saldo"] as? Int
                    let formatter = NumberFormatter()
                    formatter.locale = Locale(identifier: "id_ID")
                            formatter.groupingSeparator = "."
                            formatter.numberStyle = .decimal
                    
                    let formattedNominalTargetAmount = formatter.string(from: wallet as! NSNumber) ?? ""
                    
                    self.username.text = usernameData
                    self.email.text = emailData
                    self.noTelp.text = noTelpData
                    self.role.text = "user"
                    self.saldo.text = "Rp \(formattedNominalTargetAmount)"
                    
                }
                
                print(itemObject)
            
            case .failure(let error):
                print(error)
            }
        })
    }
    
    @objc func buttonLogOutTapped(){
        let alertController = UIAlertController(title: "Log Out", message: "Aakah kamu yakin ingin keluar?", preferredStyle: .alert);
                
                alertController.addAction(UIAlertAction(title: "Batal", style: .default, handler: {action in }))
                present(alertController, animated: true, completion: nil)
                
                alertController.addAction(UIAlertAction(title: "Log Out", style: .default,handler: {action in
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                    
                }))

    }

}
