//
//  LoginController.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 09/12/22.
//

import UIKit
import Kingfisher
import Alamofire

class LoginController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var pageNameLabel: UILabel!
//    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var regexUsernameLabel: UILabel!
//    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var regexPasswordLabel: UILabel!
    @IBOutlet weak var signupLabel: UILabel!
    
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var usernameTableField: UITextField!
    @IBOutlet weak var passwordTableField: UITextField!
    
    var username = ""
    var password = ""
    var eyeClick = true
    let userDefaultManager = UserDefaultManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.isHidden = false
        loginButton.isEnabled = false
        
        pageNameLabel.text = "Hello,"
//        usernameLabel.text = "Username"
//        passwordLabel.text = "Password"
        signupLabel.text = "Don't Have an Account? "
        regexUsernameLabel.isHidden = true
        regexPasswordLabel.isHidden = true
        
        usernameTableField.delegate = self
        passwordTableField.delegate = self
        
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        
        registerButton.addTarget(self, action: #selector(signupButtonTaped), for: .touchUpInside)
        
        isLoggedIn()
    }
    
    
    @IBAction func showingPassword(_ sender: Any) {
        if eyeClick{
            passwordTableField.isSecureTextEntry = false
        }
        else {
            passwordTableField.isSecureTextEntry = true
        }
        eyeClick = !eyeClick
    }
    
    @IBAction func regexPassword(_ sender: Any) {
        if let password = passwordTableField.text{
            if let errorMessage = invalidPassword(password){
                regexPasswordLabel.text = errorMessage
                regexPasswordLabel.isHidden = false
            }
            else {
                regexPasswordLabel.isHidden = true
            }
        }
        checkValidation()
    }

    func invalidPassword(_ value: String) -> String? {
            if (value.count<6) {
                return "Password must contain more than 6 characters!"
            }
            if !containsDigit(value) {
                return "Password must contain number!"
            }
            if !containsLowerCase(value) {
                return "Password must contain a lowercase alphabet!"
            }
            if !containsUpperCase(value) {
                return "Password must contain an uppercase alphabet!"
            }
            return nil
        }

        func containsDigit(_ value: String) -> Bool {
            let regex = ".*[0-9].*"
            let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
            return predicate.evaluate(with: value)
        }

        func containsLowerCase(_ value: String) -> Bool {
            let regex = ".*[a-z].*"
            let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
            return predicate.evaluate(with: value)
        }

        func containsUpperCase(_ value: String) -> Bool {
            let regex = ".*[A-Z].*"
            let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
            return predicate.evaluate(with: value)
        }

        func checkValidation() {
            if regexPasswordLabel.isHidden {
                loginButton.isEnabled = true
            }
            else {
                loginButton.isEnabled = false
            }
        }
        
        @objc func loginButtonTapped() {
            let param = ["username": usernameTableField.text, "password": passwordTableField.text]
            
            AF.request("http://localhost:8080/login", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: {response in switch response.result {
            case.success :
                let itemObject = response.value as? [String: Any]
                let data = itemObject? ["data"] as? [String: Any]
                let token = data? ["token"] as? String
                let role = data? ["jwtrole"] as? String
                UserDefaults.standard.setValue(token, forKey: "loginToken")
                UserDefaults.standard.synchronize()
                
                if (role == "user") {
                    self.userDefaultManager.setAuthToken(token: token!)
                    
                    print (token)
                    let homepageController = self.storyboard?.instantiateViewController(withIdentifier: "HomepageController") as! HomepageController
                    self.navigationController?.pushViewController(homepageController, animated: true)
                }
                else {
                    let alert = UIAlertController(title: "Uh Oh!", message: "You're not allowed to acces this page!", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            case.failure(let error):
                print (error)
            }})
         }
        
        @objc func signupButtonTaped() {
            let registerController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterController") as! RegisterController
            self.navigationController?.pushViewController(registerController, animated: true)
        }
    
    func isLoggedIn(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as? String
        
        if token ?? "" != "" {
            let homePageController = self.storyboard?.instantiateViewController(withIdentifier: "HomepageController") as! HomepageController
            self.navigationController?.pushViewController(homePageController, animated: true)
        }
    }
}

