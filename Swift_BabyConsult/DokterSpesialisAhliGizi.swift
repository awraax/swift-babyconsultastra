//
//  DokterSpesialisAhliGizi.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 21/12/22.
//

import UIKit
import Alamofire

class DokterSpesialisAhliGizi: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var ahliGiziView: UIView!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var dokterAhliGiziImage: UIImageView!
    @IBOutlet weak var pageNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dokterGiziTable: UITableView!
    
    var arrayItem: [[String : Any]] = [[:]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        dokterGiziTable.delegate = self
        dokterGiziTable.dataSource = self
        pageNameLabel.text = "Dokter Spesialis Anak"
        descriptionLabel.text = "Dokter yang berfokus pada kesehatan anak."
        buttonBack.addTarget(self, action: #selector(buttonBackNutritionist), for: .touchUpInside)
        
//        ahliGiziView.layer.borderWidth = 0.5
//        ahliGiziView.layer.borderColor = UIColor.black.cgColor
        ahliGiziView.layer.cornerRadius = 5.0
        
        getRequestNutritionist()

    }
    
    @objc func buttonBackNutritionist(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrayItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath)
        let labelNamaDokter = cell2.viewWithTag(8) as! UILabel
        let labelJenisKelamin = cell2.viewWithTag(7) as! UILabel
        let labelAsalRumahSakit = cell2.viewWithTag(6) as! UILabel
        let labelJadwal = cell2.viewWithTag(5) as! UILabel
        
        let namaDokterLabel = cell2.viewWithTag(4) as! UILabel
        let jenisKelaminLabel = cell2.viewWithTag(3) as! UILabel
        let asalRumahSakitLabel = cell2.viewWithTag(2) as! UILabel
        let jadwalLabel = cell2.viewWithTag(1) as! UILabel
        
        labelNamaDokter.text = "Nama Dokter  :"
        labelJenisKelamin.text = "Jenis Kelamin  :"
        labelAsalRumahSakit.text = "RS Asal          :"
        labelJadwal.text = "Jadwal           :"
        
        let itemLabel = arrayItem[indexPath.row]
        let namaDokter = itemLabel["namaDokter"] as? String
        let jenisKelamin = itemLabel["jenisKelamin"] as? String
        let asalRumahSakit = itemLabel["asalRumahSakit"] as? String
        let jadwal = itemLabel["jadwal"] as? String
        
        cell2.layer.borderWidth = 0.5
        cell2.layer.borderColor = UIColor.black.cgColor
        
        namaDokterLabel.text = namaDokter
        jenisKelaminLabel.text = jenisKelamin
        asalRumahSakitLabel.text = asalRumahSakit
        jadwalLabel.text = jadwal
        
        return cell2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }

    func getRequestNutritionist(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/baby-consult/data-doctor/1", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any]
                let code = itemObject?["status"] as? Int
                let data = itemObject?["data"] as? [[String : Any]]
                self.arrayItem = data!
                self.dokterGiziTable.reloadData()
                
                if code ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                }
                
                print(itemObject!)
            
            case .failure(let error):
                print(error)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["totalHarga" : "45000","doctor_id" : 1] as [String : Any]
        
        AF.request("http://localhost:8080/baby-consult/transaction", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler:  { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any]
                let code = itemObject?["status"] as? Int
    //                let success = itemObject?["success"] as? Bool
                let data = itemObject?["data"] as? [String: Any]
                let status = itemObject?["status"] as? Bool
    //                let total = data?["totalHarga"] as! String
                
                if status == true {
                    let paymentSuccess = self.storyboard?.instantiateViewController(withIdentifier: "MakeTransactionController") as! MakeTransactionController

                    self.navigationController?.pushViewController(paymentSuccess, animated: true)

                } else {
                    let paymentFailed = self.storyboard?.instantiateViewController(withIdentifier: "HomepageController") as! HomepageController
                    self.navigationController?.pushViewController(paymentFailed, animated: true)
                }

            case .failure(let error):
                print(error)
            }
                
        })
    }
}
