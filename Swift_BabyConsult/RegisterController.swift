//
//  RegisterController.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 09/12/22.
//

import UIKit
import Alamofire
import Kingfisher


class RegisterController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var pageNameLabel: UILabel!
    @IBOutlet weak var pageDownLabel: UILabel!
    @IBOutlet weak var pageCaptionRegister: UILabel!
    @IBOutlet weak var warningPassword: UILabel!
    @IBOutlet weak var warningUsername: UILabel!
    
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginBackButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerButton.isEnabled = false
        warningPassword.isHidden = true
        warningUsername.isHidden = true
        
        pageNameLabel.text = "Sign Up"
        pageDownLabel.text = "Already have an account?"
        pageCaptionRegister.text = "Let's make your account here!"
        warningUsername.text = "Username tidak sesuai"
        warningPassword.text = "Password lemah, tingkatkan lagi keamanan password"
        
        usernameTF.delegate = self
        emailTF.delegate = self
        phoneNumberTF.delegate = self
        passwordTF.delegate = self
        
        registerButton.addTarget(self, action: #selector(buttonRegisterTapped), for: .touchUpInside)
        loginBackButton.addTarget(self, action: #selector(buttonBackTapped), for: .touchUpInside)
    }
    
    @objc func buttonRegisterTapped(){
        let param = ["username" : usernameTF.text, "email" : emailTF.text, "noTelp" : phoneNumberTF.text, "password" : passwordTF.text]
        AF.request("http://localhost:8080/register", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in switch response.result {
        case .success:
            let itemObject = response.value as? [String : Any]
            let status = itemObject? ["status"] as? Bool
            let message = itemObject? ["message"] as? String
            
            if status! {
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Registration Success!", message: "Akunmu berhasil dibuat", preferredStyle: .alert);
                    alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                let alertController = UIAlertController(title: "Registration Failed!", message: nil, preferredStyle: .alert);
                alertController.addAction(UIAlertAction(title: "Try Again", style: .default,handler: nil));
                self.present(alertController, animated: true, completion: nil)
            }
        case.failure(let error):
            print(error)
            }
        })
    }
    
    @objc func buttonBackTapped() {
           self.navigationController?.popViewController(animated: true)
       }

    
    @IBAction func verifUsername(_ sender: Any) {
        if let username = usernameTF.text
                {
                if let errorMsg = invalidUsername(username) {
                    warningUsername.text = errorMsg
                    warningUsername.isHidden = false
                }
                else {
                    warningUsername.isHidden = true
                }
            }
    }
    
    @IBAction func verifPassword(_ sender: Any) {
    if let password = passwordTF.text {
            if let errorMsg = invalidPassword(password) {
                warningPassword.text = errorMsg
                warningPassword.isHidden = false
            }
            else {
                warningPassword.isHidden = true
            }
        }
        cekValidationForm()

    }
    
    func invalidPassword(_ value: String) -> String? {
    if (value.count < 8) {
        return "Password minimal 8 karakter"
    }
    if !containsDigit(value) {
        return "Password harus menggunakan angka"
    }
    if !containsLowerCase(value) {
        return "Password harus menggunakan huruf kecil"
    }
    if !containsUpperCase(value) {
        return "Password harus menggunakan huruf kapital"
    }
    return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regex = ".*[0-9].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func containsLowerCase(_ value: String) -> Bool {
        let regex = ".*[a-z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func containsUpperCase (_ value: String) -> Bool {
        let regex = ".*[A-Z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func cekValidationForm() {
        if warningUsername.isHidden && !usernameTF.text!.isEmpty && warningPassword.isHidden{
                registerButton.isEnabled = true
            }
            else {
                registerButton.isEnabled = false
            }
        }

    
    func invalidUsername(_ value: String) -> String? {
        if (value.count < 4) {
            return "Username minimal 4 karakter"
        }
        return nil
    }

    
}
