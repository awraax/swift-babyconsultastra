//
//  PatientProblemTableViewCell.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 30/12/22.
//

import UIKit

class PatientProblemTableViewCell: UITableViewCell {

    @IBOutlet weak var patientLabel: UILabel!
    @IBOutlet weak var detailProblem: UILabel!
//    @IBOutlet weak var constrainData: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
