//
//  TransaksiTableViewCell.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 27/12/22.
//

import UIKit

class TransaksiTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource   {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryTF: UITextField!
    
    var categoryDropdown = UIPickerView()
    var categoryArray = [String]()
    var idCategoryArr = [Int]()
    var doneButtonTappedCallback: ((Int) -> ())?
    var idKategori: Int = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Toolbar
                let buttonDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneFunction))
                
                let barAccessory = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
                barAccessory.barStyle = .default
                barAccessory.isTranslucent = false
                barAccessory.items = [buttonDone]
                self.categoryTF.inputAccessoryView = barAccessory
                
                //pickerview
                categoryDropdown.delegate = self
                print(categoryArray)
                print(idCategoryArr)
                
        
                self.categoryTF.inputView = categoryDropdown
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }

    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
            return categoryArray.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return categoryArray[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        categoryTF.text = "  \(categoryArray[row])"
        idKategori = idCategoryArr[row]
    }
    
    @objc func doneFunction() {
            doneButtonTappedCallback?(idKategori)
    }
    
}
