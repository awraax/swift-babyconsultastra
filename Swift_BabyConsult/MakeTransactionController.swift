//
//  MakeTransactionController.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 23/12/22.
//

import UIKit
import Alamofire

class MakeTransactionController: UIViewController{
    @IBOutlet weak var penggunaView: UIView!
    @IBOutlet weak var metodeView: UIView!
    
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var paymentButton: UIButton!
    
    @IBOutlet weak var namePageLabel: UILabel!
    @IBOutlet weak var transactionPageLabel: UILabel!
    @IBOutlet weak var namaPasien: UILabel!
    @IBOutlet weak var noTelp: UILabel!
    @IBOutlet weak var namaPasienLabel: UILabel!
    @IBOutlet weak var noTelpLabel: UILabel!
    @IBOutlet weak var hargaLabel: UILabel!
    @IBOutlet weak var totalHarga: UILabel!
    @IBOutlet weak var saldo: UILabel!
    
    var currentUser: [[String : Any]] = []
    var idTransaksi: Int = 0
    
    var categoryArray: [String] = []
    var idCategoryArray = [Int]()
    var kategoriCell:[String] = []
    var allCategory: [[String : Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        namaPasien.text = "Nama Pasien     :"
        noTelp.text = "No. Telp            :"
        hargaLabel.text = "Harga           :"
        
//        pembayaranSwitch.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
            
        self.navigationController?.navigationBar.isHidden = true
        buttonBack.addTarget(self, action: #selector(buttonBackTransaction), for: .touchUpInside)
        
        penggunaView.layer.borderWidth = 0.5
        penggunaView.layer.borderColor = UIColor.black.cgColor
        penggunaView.layer.cornerRadius = 5.0
        
        metodeView.layer.borderWidth = 0.5
        metodeView.layer.borderColor = UIColor.black.cgColor
        metodeView.layer.cornerRadius = 5.0
        
        getRequestGetUser()
        paymentButton.addTarget(self, action: #selector(buttonPayment), for: .touchUpInside)
        getRequestPayment()
    }
    
    
    @objc func buttonBackTransaction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getRequestGetUser(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/baby-consult/current_user", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler:  { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any]
                let code = itemObject?["status"] as? Int
                let data = itemObject?["data"] as? [String: Any]
                let username = data?["username"] as! String
                let noHp = data?["noTelp"] as! String
                let saldoData = data?["wallet"] as? [String : Any]
                let wallet = saldoData?["saldo"] as? Int
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "id_ID")
                        formatter.groupingSeparator = "."
                        formatter.numberStyle = .decimal
                
                let formattedNominalTargetAmount = formatter.string(from: wallet as! NSNumber) ?? ""
                
                self.namaPasienLabel.text = username
                self.noTelpLabel.text = noHp
                self.saldo.text = "Rp \(formattedNominalTargetAmount)"
                
            case .failure(let error):
                print(error)
            }
                
            })
    }
    
    func getRequestPayment(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]

        AF.request("http://localhost:8080/baby-consult/transaction-history/current-user", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler:  { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any] ?? [:]
                let code = itemObject["status"] as? Int
                let data = itemObject["data"] as? [[String: Any]]
                let firstData = data![0] as? [String : Any]
                let totalHargaData = firstData?["totalHarga"] as? Double
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "id_ID")
                        formatter.groupingSeparator = "."
                        formatter.numberStyle = .decimal
                
                let formattedNominalTargetAmount = formatter.string(from: totalHargaData as! NSNumber) ?? ""
                
                
                self.totalHarga.text = "Rp \(formattedNominalTargetAmount)"

            case .failure(let error):
                print(error)
            }

            })
    }
    
    @objc func buttonPayment(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/baby-consult/payment/\(idTransaksi)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler:  { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any]
                let code = itemObject?["status"] as? Int
                let data = itemObject?["data"] as? [String: Any]
                let status = itemObject?["status"] as? Bool
                
                if status == true {
                    let paymentSuccess = self.storyboard?.instantiateViewController(withIdentifier: "PaymentTransaction") as! PaymentTransaction

                    self.navigationController?.pushViewController(paymentSuccess, animated: true)
                    

                } else {
                    let paymentFailed = self.storyboard?.instantiateViewController(withIdentifier: "HomepageController") as! HomepageController
                    self.navigationController?.pushViewController(paymentFailed, animated: true)
                    
                }
                
            case .failure(let error):
                print(error)
            }
                
            })
    }
    

}
