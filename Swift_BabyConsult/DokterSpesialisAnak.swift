//
//  DokterSpesialisAnak.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 21/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class DokterSpesialisAnak: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var anakView: UIView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pageNameLabel: UILabel!
    @IBOutlet weak var dokterAnakTable: UITableView!
    @IBOutlet weak var dokterAnakImage: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    var arrayItem: [[String : Any]] = [[:]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        dokterAnakTable.delegate = self
        dokterAnakTable.dataSource = self
        pageNameLabel.text = "Dokter Spesialis Anak"
        descriptionLabel.text = "Dokter yang berfokus pada kesehatan anak."
        backButton.addTarget(self, action: #selector(buttonBackPediatrician), for: .touchUpInside)
        
//        anakView.layer.borderWidth = 0.5
//        anakView.layer.borderColor = UIColor.black.cgColor
        anakView.layer.cornerRadius = 5.0
        
        getRequestPediatrician()

    }
    
    @objc func buttonBackPediatrician(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrayItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)
        let labelNamaDokter = cell1.viewWithTag(4) as! UILabel
        let labelJenisKelamin = cell1.viewWithTag(3) as! UILabel
        let labelAsalRumahSakit = cell1.viewWithTag(2) as! UILabel
        let labelJadwal = cell1.viewWithTag(1) as! UILabel
        
        let namaDokterLabel = cell1.viewWithTag(5) as! UILabel
        let jenisKelaminLabel = cell1.viewWithTag(6) as! UILabel
        let asalRumahSakitLabel = cell1.viewWithTag(7) as! UILabel
        let jadwalLabel = cell1.viewWithTag(8) as! UILabel
        
        cell1.layer.borderWidth = 0.5
        cell1.layer.borderColor = UIColor.black.cgColor
        
        labelNamaDokter.text = "Nama Dokter  :"
        labelJenisKelamin.text = "Jenis Kelamin  :"
        labelAsalRumahSakit.text = "RS Asal          :"
        labelJadwal.text = "Jadwal           :"
        
        let itemLabel = arrayItem[indexPath.row]
        let namaDokter = itemLabel["namaDokter"] as? String
        let jenisKelamin = itemLabel["jenisKelamin"] as? String
        let asalRumahSakit = itemLabel["asalRumahSakit"] as? String
        let jadwal = itemLabel["jadwal"] as? String
        
        namaDokterLabel.text = namaDokter
        jenisKelaminLabel.text = jenisKelamin
        asalRumahSakitLabel.text = asalRumahSakit
        jadwalLabel.text = jadwal
        
        return cell1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func getRequestPediatrician(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/baby-consult/data-doctor/2", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any]
                let code = itemObject?["status"] as? Int
                let data = itemObject?["data"] as? [[String : Any]]
                self.arrayItem = data!
                self.dokterAnakTable.reloadData()
                
                if code ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                }
                
                print(itemObject!)
            
            case .failure(let error):
                print(error)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["totalHarga" : "45000","doctor_id" : 2] as [String : Any]
        
        AF.request("http://localhost:8080/baby-consult/transaction", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler:  { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String: Any]
                let code = itemObject?["status"] as? Int
//                let success = itemObject?["success"] as? Bool
                let data = itemObject?["data"] as? [String: Any]
                let idTransaksiData = data?["transaksi_id"] as? Int ?? 0
                let status = itemObject?["status"] as? Bool
//                let total = data?["totalHarga"] as! String
                
                if status == true {
                    let paymentSuccess = self.storyboard?.instantiateViewController(withIdentifier: "MakeTransactionController") as! MakeTransactionController
                    
                    paymentSuccess.idTransaksi = idTransaksiData
                    
                    self.navigationController?.pushViewController(paymentSuccess, animated: true)

                } else {
                    let paymentFailed = self.storyboard?.instantiateViewController(withIdentifier: "HomepageController") as! HomepageController
                    self.navigationController?.pushViewController(paymentFailed, animated: true)
                }

            case .failure(let error):
                print(error)
            }
                
    })
    }
}
