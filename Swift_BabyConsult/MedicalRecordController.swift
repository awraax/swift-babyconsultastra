//
//  MedicalRecordController.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 04/01/23.
//

import UIKit
import Alamofire

class MedicalRecordController: UIViewController {

    @IBOutlet weak var keluhanLabel: UILabel!
    @IBOutlet weak var diagnosisLabel: UILabel!
    @IBOutlet weak var pengobatanLabel: UILabel!
    @IBOutlet weak var labelKeluhan: UILabel!
    @IBOutlet weak var labelPengobatan: UILabel!
    @IBOutlet weak var labelDiagnosis: UILabel!
    
    @IBOutlet weak var backButtonMedRec: UIButton!
    
    var idRecord: Int = 1
    var data: [String: Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        backButtonMedRec.addTarget(self, action: #selector(buttonBackMedicalRecord), for: .touchUpInside)
        
        keluhanLabel.text = "Keluhan     :"
        diagnosisLabel.text = "Diagnosis     :"
        pengobatanLabel.text = "Pengobatan     :"
        
        getRequestMedicalRecord()
    }

    @objc func buttonBackMedicalRecord(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getRequestMedicalRecord(){
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/baby-consult/medical-record/current-user/\(idRecord)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler:  { response in switch response.result {
            case .success:
                let itemObject = response.value as? [String : Any] ?? [:]
                let code = itemObject["status"] as? Int
                
                if code ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "loginToken")
                    self.navigationController?.popToRootViewController(animated: false)
                } else{
                    let data = itemObject["data"] as? [String : Any]
                    let keluhan = data!["keluhanPasien"] as? String
                    let rekamMedis = data?["record"] as? [String : Any]
                    let diagnosis = rekamMedis?["diagnosis"] as? String
                    let pengobatan = rekamMedis?["pengobatan"] as? String
                    
                    self.labelKeluhan.text = keluhan
                    self.labelDiagnosis.text = diagnosis
                    self.labelPengobatan.text = pengobatan
                }
                
                
            case .failure(let error):
                print(error)
            }
                
            })
    }

    
}
