//
//  UserDefaultManager.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 21/12/22.
//

import Foundation
struct UserDefaultManager{
    let userDefault = UserDefaults.standard
    
    func setAuthToken(token: String){
        userDefault.set(token, forKey: "token")
    }
    
    func getAuthToken()-> String{
        return userDefault.string(forKey: "token") ?? ""
    }
}
