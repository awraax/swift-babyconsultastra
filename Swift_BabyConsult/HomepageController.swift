//
//  HomepageController.swift
//  Swift_BabyConsult
//
//  Created by ADA-NB182 on 09/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class HomepageController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var historiView: UIView!
    @IBOutlet weak var rekamView: UIView!
    @IBOutlet weak var spesialisAnakView: UIView!
    @IBOutlet weak var ahliGiziView: UIView!
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var chatDokterButton: UIButton!
    @IBOutlet weak var historiTransactionButton: UIButton!
    @IBOutlet weak var rekamMedisButton: UIButton!
    @IBOutlet weak var dokterAnakButton: UIButton!
    @IBOutlet weak var dokterGiziButton: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!

    var arrayImageCell : [UIImage] = [UIImage(named :"artikelSatu")!,
                                       UIImage(named: "artikelDua")!,
                                       UIImage(named: "artikelTiga")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        collectionView.delegate = self
        collectionView.dataSource = self

        profileButton.addTarget(self, action: #selector(profileButtonTaped), for: .touchUpInside)
        
        chatDokterButton.addTarget(self, action: #selector(chatDokterButtonTapped), for: .touchUpInside)
        
        historiTransactionButton.addTarget(self, action: #selector(historiTransactionButtonTapped), for: .touchUpInside)
        
        rekamMedisButton.addTarget(self, action: #selector(listMedicalRecordTapped), for: .touchUpInside)
        
        dokterAnakButton.addTarget(self, action: #selector(dokterAnakButtonTapped), for: .touchUpInside)
        
        dokterGiziButton.addTarget(self, action: #selector(dokterAhliGiziButtonTapped), for: .touchUpInside)
        
        chatView.layer.borderWidth = 0.5
        chatView.layer.borderColor = UIColor.black.cgColor
        chatView.layer.cornerRadius = 5.0
        
        historiView.layer.cornerRadius = 5.0
        historiView.layer.borderWidth = 0.5
        historiView.layer.borderColor = UIColor.black.cgColor
        
        rekamView.layer.cornerRadius = 5.0
        rekamView.layer.borderWidth = 0.5
        rekamView.layer.borderColor = UIColor.black.cgColor
        
        spesialisAnakView.layer.cornerRadius = 5.0
        spesialisAnakView.layer.borderWidth = 0.5
        spesialisAnakView.layer.borderColor = UIColor.black.cgColor
        
        ahliGiziView.layer.cornerRadius = 5.0
        ahliGiziView.layer.borderWidth = 0.5
        ahliGiziView.layer.borderColor = UIColor.black.cgColor
        
//        collectionView.layer.cornerRadius = 5.0
//        collectionView.layer.borderWidth = 0.5
//        collectionView.layer.borderColor = UIColor.black.cgColor
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImageCell.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath)
        let imageCell = cell.viewWithTag(1) as! UIImageView
        imageCell.image = arrayImageCell[indexPath.row]
        return cell
    }
    
    @objc func profileButtonTaped(){
        let profileController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileController")
                    
        self.navigationController?.pushViewController(profileController!, animated: true)
    }
    
    @objc func chatDokterButtonTapped(){
//        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
//        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
//        let param = ["totalHarga" : "45000"]
//
//        AF.request("http://localhost:8080/baby-consult/transaction", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
//            .responseJSON(completionHandler:  { response in switch response.result {
//            case .success:
//                let itemObject = response.value as? [String: Any]
//                let code = itemObject?["status"] as? Int
////                let success = itemObject?["success"] as? Bool
//                let data = itemObject?["data"] as? [String: Any]
//                let status = itemObject?["status"] as? Bool
////                let total = data?["totalHarga"] as! String
//
//                if status == true {
//                    let paymentSuccess = self.storyboard?.instantiateViewController(withIdentifier: "MakeTransactionController") as! MakeTransactionController
//
//                    self.navigationController?.pushViewController(paymentSuccess, animated: true)
//
//                } else {
//                    let paymentFailed = self.storyboard?.instantiateViewController(withIdentifier: "HomepageController") as! HomepageController
//                    self.navigationController?.pushViewController(paymentFailed, animated: true)
//                }
//
//            case .failure(let error):
//                print(error)
//            }
//
//    })
        let makeConsultationForum = self.storyboard?.instantiateViewController(withIdentifier: "ConsultationForum")

        self.navigationController?.pushViewController(makeConsultationForum!, animated: true)
        
    }
    
    @objc func historiTransactionButtonTapped(){
        let transactionHistory = self.storyboard?.instantiateViewController(withIdentifier: "TransactionHistory")
                    
        self.navigationController?.pushViewController(transactionHistory!, animated: true)
    }
    
    @objc func listMedicalRecordTapped(){
        let medicalRecord = self.storyboard?.instantiateViewController(withIdentifier: "ListMedicalRecord")
                    
        self.navigationController?.pushViewController(medicalRecord!, animated: true)
    }
    
    @objc func dokterAnakButtonTapped(){
        let dokterSpesialisAnak = self.storyboard?.instantiateViewController(withIdentifier: "DokterSpesialisAnak")
                    
        self.navigationController?.pushViewController(dokterSpesialisAnak!, animated: true)
    }
    
    @objc func dokterAhliGiziButtonTapped(){
        let dokterSpesialisAhliGizi = self.storyboard?.instantiateViewController(withIdentifier: "DokterSpesialisAhliGizi")
                    
        self.navigationController?.pushViewController(dokterSpesialisAhliGizi!, animated: true)
    }
    
    
}
